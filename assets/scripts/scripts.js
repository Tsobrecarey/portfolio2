const homeBtn = document.getElementById("homeBtn");
const aboutBtn = document.getElementById("aboutBtn");
const skillsBtn = document.getElementById("skillsBtn");
const projectsBtn = document.getElementById("projectsBtn");
const contactsBtn = document.getElementById("contactsBtn");

let home = document.getElementById("home");
let homeAbout = document.getElementById("homeAbout");
let homeSkills = document.getElementById("homeSkills");
let homeProjects = document.getElementById("homeProjects");
let homeContacts = document.getElementById("homeContacts");

homeBtn.addEventListener("click", function () {
  home.classList.add("opaque");
  homeBtn.classList.add("selected");
  homeImg.classList.add("opaque");
  aboutBtn.classList.remove("selected");
  skillsBtn.classList.remove("selected");
  projectsBtn.classList.remove("selected");
  contactsBtn.classList.remove("selected");
  homeAbout.classList.remove("opaque");
  homeSkills.classList.remove("opaque");
  homeProjects.classList.remove("opaque");
  homeContacts.classList.remove("opaque");
  aboutImg.classList.remove("opaque");
  skillImg.classList.remove("opaque");
  projectImg.classList.remove("opaque");
  contactImg.classList.remove("opaque");
});

aboutBtn.addEventListener("click", function () {
  aboutBtn.classList.add("selected");
  homeAbout.classList.add("opaque");
  aboutImg.classList.add("opaque");
  homeBtn.classList.remove("selected");
  skillsBtn.classList.remove("selected");
  projectsBtn.classList.remove("selected");
  contactsBtn.classList.remove("selected");
  home.classList.remove("opaque");
  homeSkills.classList.remove("opaque");
  homeProjects.classList.remove("opaque");
  homeContacts.classList.remove("opaque");
  homeImg.classList.remove("opaque");
  skillImg.classList.remove("opaque");
  projectImg.classList.remove("opaque");
  contactImg.classList.remove("opaque");
});

skillsBtn.addEventListener("click", function () {
  skillsBtn.classList.add("selected");
  homeSkills.classList.add("opaque");
  skillImg.classList.add("opaque");
  homeBtn.classList.remove("selected");
  aboutBtn.classList.remove("selected");
  projectsBtn.classList.remove("selected");
  contactsBtn.classList.remove("selected");
  home.classList.remove("opaque");
  homeAbout.classList.remove("opaque");
  homeProjects.classList.remove("opaque");
  homeContacts.classList.remove("opaque");
  homeImg.classList.remove("opaque");
  aboutImg.classList.remove("opaque");
  projectImg.classList.remove("opaque");
  contactImg.classList.remove("opaque");
});

projectsBtn.addEventListener("click", function () {
  projectsBtn.classList.add("selected");
  homeProjects.classList.add("opaque");
  projectImg.classList.add("opaque");
  homeBtn.classList.remove("selected");
  aboutBtn.classList.remove("selected");
  skillsBtn.classList.remove("selected");
  contactsBtn.classList.remove("selected");
  home.classList.remove("opaque");
  homeAbout.classList.remove("opaque");
  homeSkills.classList.remove("opaque");
  homeContacts.classList.remove("opaque");
  homeImg.classList.remove("opaque");
  aboutImg.classList.remove("opaque");
  skillImg.classList.remove("opaque");
  contactImg.classList.remove("opaque");
});

contactsBtn.addEventListener("click", function () {
  contactsBtn.classList.add("selected");
  homeContacts.classList.add("opaque");
  contactImg.classList.add("opaque");
  homeBtn.classList.remove("selected");
  aboutBtn.classList.remove("selected");
  skillsBtn.classList.remove("selected");
  projectsBtn.classList.remove("selected");
  home.classList.remove("opaque");
  homeAbout.classList.remove("opaque");
  homeSkills.classList.remove("opaque");
  homeProjects.classList.remove("opaque");
  homeImg.classList.remove("opaque");
  aboutImg.classList.remove("opaque");
  skillImg.classList.remove("opaque");
  projectImg.classList.remove("opaque");
});
